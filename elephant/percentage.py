from collections import namedtuple


def percentage(value):
    return Percentage(float(value) / 100)


class Percentage(namedtuple('Percentage', 'value')):

    def __repr__(self):
        return f"{self.value:.2%}"
