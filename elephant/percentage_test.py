from elephant.percentage import percentage


def test_percentage_should_render():
    assert str(percentage(4)) == "4.00%"
