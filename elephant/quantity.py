from collections import namedtuple


def qty(value):
    return Quantity(int(value))


class Quantity(namedtuple('Quantity', 'value')):

    def __repr__(self):
        return f"qty({self.value:})"
