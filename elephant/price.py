from collections import namedtuple
from functools import total_ordering


def _parse_price(price):
    if price.startswith("$"):
        return Price(float(price[1:]))
    return Price(float(price))


def price(value):
    if isinstance(value, str):
        return _parse_price(value)
    return Price(value)


@total_ordering
class Price(namedtuple('Price', 'amount')):

    def qty(self, qty):
        return Price(self.amount * qty.value)

    def inflate(self, percentage):
        return Price(self.amount * (1 + percentage.value))

    def reduce(self, percentage):
        return Price(self.amount * (1 - percentage.value))

    def __eq__(self, other):
        return self.amount == other.amount

    def __lt__(self, other):
        return self.amount < other.amount

    def __str__(self):
        return f"${self.amount:.2f}"

    def __repr__(self):
        return f"${self.amount:.2f}"
