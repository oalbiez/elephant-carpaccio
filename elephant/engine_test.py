from elephant.engine import compute
from elephant.price import price
from elephant.quantity import qty
from elephant.states import States


def test_should_return_price_when_no_item():
    assert price_for(qty(0), "$4.00", state('CA')) == "$0.00"


def test_should_return_price_when_multiples_items():
    assert price_for(qty(3), "$4.00", state('CA')) == "$12.99"


def test_should_return_rounded_price():
    assert price_for(qty(1), "$23.00", state('UT')) == "$24.58"


def test_should_compute_taxe_for_contries():
    assert price_for(qty(1), "$1.00", state('UT')) == "$1.07"
    assert price_for(qty(1), "$1.00", state('NV')) == "$1.08"
    assert price_for(qty(1), "$1.00", state('TX')) == "$1.06"
    assert price_for(qty(1), "$1.00", state('AL')) == "$1.04"


def test_should_compute_discount():
    assert price_for(qty(1), "$1000.00", state('CA')) == "$1050.03"
    assert price_for(qty(1), "$5000.00", state('CA')) == "$5141.88"
    assert price_for(qty(1), "$7000.00", state('CA')) == "$7047.07"
    assert price_for(qty(1), "$10000.00", state('CA')) == "$9742.50"
    assert price_for(qty(1), "$50000.00", state('CA')) == "$46006.25"


def test_acceptance():
    assert price_for(qty(978), "$270.99", state('UT')) == "$240705.26"


def state(value):
    return States.parse(value)


def price_for(quantity, unit_price, state):
    return str(compute(quantity, price(unit_price), state))
