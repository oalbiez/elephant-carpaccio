from elephant.percentage import percentage
from elephant.price import price
from elephant.states import States


TAXES = {
    'AL': States.AL,
    'CA': States.CA,
    'NV': States.NV,
    'TX': States.TX,
    'UT': States.UT,
}


def tax_for(state):
    return state.tax


DISCOUNTS = [
    (lambda p: p < price(1000), percentage(0)),
    (lambda p: p < price(5000), percentage(3.00)),
    (lambda p: p < price(7000), percentage(5.00)),
    (lambda p: p < price(10000), percentage(7.00)),
    (lambda p: p < price(50000), percentage(10.00)),
    (lambda p: True, percentage(15.00)),
]


def discount(price):
    for predicate, discount in DISCOUNTS:
        if predicate(price):
            return price.reduce(discount)
    return price


def compute(quantity, price, state):
    return discount(price.qty(quantity)).inflate(tax_for(state))
