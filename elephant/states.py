from enum import Enum

from elephant.percentage import percentage


class States(Enum):
    AL = ('Alabhama', percentage(4.00))
    CA = ('California', percentage(8.25))
    NV = ('Nevada', percentage(8.00))
    TX = ('Texas', percentage(6.25))
    UT = ('Utha', percentage(6.85))

    def __init__(self, state_name, tax):
        self.state_name = state_name
        self.tax = tax

    @classmethod
    def parse(cls, definition):
        for state in States:
            if definition.upper() == state.name:
                return state
        raise NameError(definition)
